<?php

class HomeController extends Controller
{
	public function process($params)
	{
		// HTML header
		$this->head = array(
			'title' => 'Home Page',
			'description' => ''
		);
		
        // Sets the template
        $this->view = 'home';
	}
}