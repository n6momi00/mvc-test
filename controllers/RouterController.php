<?php

class RouterController extends Controller
{
	// The inner controller instance
	protected $controller;

	public function process($params)
	{
		// The controller is the 1st URL parameter
		$parsedUrl = $this->parseUrl($params[0]);

		if (empty($parsedUrl[0]))
		{
			$this->redirect('project');
		}

		$controllerClass = $this->dashesToCamel(array_shift($parsedUrl) . 'Controller');

		// Create an instance of the class if exists, otherwise, redirect to the error page
		if (file_exists('controllers/' . $controllerClass . '.php'))
		{
			$this->controller = new $controllerClass;
		}
		else
		{
			$this->redirect('error');
		}

		// Calls the inner controller
		$this->controller->process($parsedUrl);
		// Setting template variables
		$this->data['title'] = $this->controller->head['title'];
		$this->data['description'] = $this->controller->head['description'];

		// Sets the main template
		$this->view = 'layout';
	}

	private function parseUrl($url)
	{
		// Parses URL parts into an associative array
		$parsedUrl = parse_url($url);
		// Removes the leading slash
		$parsedUrl["path"] = ltrim($parsedUrl["path"], "/");
		// Removes white-spaces around the address
		$parsedUrl["path"] = trim($parsedUrl["path"]);
		// Splits the address by slashes
		$explodedUrl = explode("/", $parsedUrl["path"]);
		return $explodedUrl;
	}

	private function dashesToCamel($text)
	{
		$text = str_replace('-', ' ', $text);
		$text = ucwords($text);
		$text = str_replace(' ', '', $text);
		return $text;
	}
}