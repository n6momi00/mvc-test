<?php

class ProjectController extends Controller
{
	public function process($params)
	{
        // Creates a model instance which allows us to access projects
        $projectManager = new ProjectManager();

        if (!empty($params[0]))
        {
        	// Retrieves an project based on its URL
	        $project = $projectManager->getProject($params[0]);
	        // If no project was found we redirect to ErrorController
	        if (!$project)
	        {
	            $this->redirect('error');
	        }

	        // HTML head
	        $this->head = array(
	            'title' => $project['title'],
	            'description' => $project['description']
	        );
	        
	        // Sets the template variables
	        $this->data = array(
	        	'title' => $project['title'],
	        	'content' => $project['content'],
	        	'author' => $project['nickname'],
	        	'date' => $project['date']
			);

	        // Sets the template
	        $this->view = 'project';
        }
        else
        {
        	// No URL was entered, so we list all of the projects
        	$projects = $projectManager->getProjects();
            $this->data['projects'] = $projects;
            $this->view = 'projects';
        }
	}
}