<?php

class LoginController extends Controller
{
    public function process($params)
    {	
    	$session = new Session();
    	if (isset($_SESSION['username']))
    	{
    		$this->redirect('home');
    	}

        $this->head['title'] = 'Login Page';

        if (isset($_SESSION['msg']))
        {
            $this->data['msg'] = $_SESSION['msg'];
            unset($_SESSION['msg']);
        }
        else
        {
            $this->data['msg'] = '';
        }

        if (isset($_POST['login-submit']))
    	{
    		if (isset($_POST['username']) && isset($_POST['password']))
	        {
                $username = $_POST['username'];
                $password = $_POST['password'];

                $login = new Login();
                $signin = $login->signin($username, $password);

                if ($signin)
                {
                    $this->redirect('project');
                }
                else
                {
                    $this->redirect('login');
                }
	        }
    	}

        // Sets the template
        $this->view = 'login';
    }
}