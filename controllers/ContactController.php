<?php

class ContactController extends Controller
{
	private $recipient = "";
	private $subject = "Email from your website";

	public function process($params)
	{
		// HTML header
                $this->head = array(
			'title' => 'Contact form',
			'description' => 'Contact us using our contact form.'
		);

                if (isset($_POST["firstname"]) && isset($_POST["lastname"]) && isset($_POST["email"]) && isset($_POST["message"]))
                {
                	$firstname = $_POST["firstname"];
                	$lastname = $_POST["lastname"];
                	$email = $_POST["email"];
                	$message = $_POST["message"];

                	$emailSender = new EmailSender();
                	$emailSender->send($recipient, $subject, $firstname, $lastname, $message, $email);
                }

                // Sets the template
                $this->view = 'contact';
	}
}