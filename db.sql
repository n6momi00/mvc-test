/**/
drop database if exists oop_db;

create database oop_db;

use oop_db;

create table user (
	user_id int primary key auto_increment,
	username varchar(255) not null unique,
	password varchar(255) not null,
	nickname varchar(255) not null,
	user_type int not null
);

create table project (
	project_id int primary key auto_increment,
	title varchar(255) not null,
	content longtext not null,
	description varchar(255) not null,
	url varchar(255) not null,
	author int not null,
	date timestamp default current_timestamp on update current_timestamp,
	foreign key (author) references user(user_id) on delete restrict
);

insert into user (username, password, nickname, user_type) values ('example', md5('example'), 'example', '1');
insert into project (title, content, description, url, author) values ('Example', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla a eros est. Quisque malesuada, metus eu sodales pulvinar, orci libero lobortis elit, eget aliquet massa erat vel nulla. Ut posuere, nulla sit amet vestibulum vestibulum, dui erat eleifend libero, viverra feugiat tellus nisi vitae orci. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Quisque consectetur bibendum massa, in scelerisque ligula aliquam vel. Nam lacus eros, finibus vitae turpis vitae, accumsan sollicitudin lectus. Curabitur ut ligula ac diam dignissim pharetra.</p><p>Cras quis iaculis lectus, a tempor felis. Sed tincidunt velit sit amet rhoncus suscipit. Donec consectetur metus porta nisi convallis blandit. Donec volutpat vitae lacus eu ullamcorper. Integer eu nisl pharetra, placerat nibh a, porta nunc. Vivamus pharetra turpis est. Quisque sit amet rutrum mi, eu ultricies urna. Duis pharetra augue in turpis maximus eleifend. Proin quis imperdiet nisi. Etiam aliquet odio lectus, a suscipit diam bibendum vel.</p><p>Nam ac urna tincidunt, auctor erat quis, consequat ex. Donec vitae elit accumsan, porta felis sit amet, tristique libero. Morbi semper mi mauris, et tempus leo dapibus ut. In mollis odio ut semper porttitor. Integer diam arcu, mollis a nibh vitae, ullamcorper bibendum tortor. Suspendisse in metus ut metus sollicitudin tincidunt eget id enim. Donec maximus tincidunt eros, et placerat felis vehicula eget. Proin elementum purus ac dolor maximus consequat.</p><p>In rutrum, eros sed convallis gravida, risus enim dignissim ante, ut ultrices mauris orci at dui. Morbi tristique, lorem eu semper lobortis, mi nulla tristique ex, vitae venenatis justo ligula quis neque. Duis volutpat mollis lorem, in pellentesque nibh pharetra vitae. Nam cursus et leo eu faucibus. Vivamus auctor placerat finibus. Praesent auctor rhoncus neque suscipit finibus. Praesent egestas leo dictum ex porttitor suscipit. Nulla mi augue, rutrum vel orci quis, malesuada placerat nibh. Mauris at nunc urna.</p><p>In at iaculis ante, ut tempor erat. Sed at sem ac orci vehicula vulputate a sit amet mauris. In auctor velit ac libero efficitur, dapibus porta odio laoreet. Aenean blandit eros vel velit porta, vitae ornare augue sodales. Etiam at tortor nibh. Sed eu faucibus justo, sed molestie quam. Donec id vulputate urna. Morbi rutrum, turpis quis placerat placerat, dui massa efficitur est, a ultricies neque augue dignissim metus. Integer egestas, ligula id interdum viverra, lacus lectus luctus nulla, eget commodo libero neque et sapien. Aliquam elit eros, eleifend euismod ultrices a, consequat ut augue. Phasellus ac laoreet metus.</p>', 'This is an example', 'example', 1);