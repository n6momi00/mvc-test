<?php

class EmailSender
{
	public function send($recipient, $subject, $firstname, $lastname, $content, $from)
	{
		$header = "From: " . $from;
		$header .= "\nMIME-Version: 1.0\n";
        $header .= "Content-Type: text/html; charset=\"utf-8\"\n";

        $message = "<p>Firstname: " . $firstname . "</p>";
        $message .= "<p>Lastname: " . $lastname . "</p>";
        $message .= "<p>Message: " . $content . "</p>";

        return mb_send_mail($recipient, $subject, $message, $header);
	}
}