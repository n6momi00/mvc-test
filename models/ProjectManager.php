<?php

class ProjectManager
{
	public function getProject($url)
	{
	    return Db::queryOne('
			SELECT `project_id`, `title`, `content`, `url`, `description`, `author`, `date`, `nickname`
			FROM `project`
			INNER JOIN `user`
			ON author = user.user_id
			WHERE `url` = ?
		', array($url));
	}

	public function getProjects()
	{
        return Db::queryAll('
			SELECT `project_id`, `title`, `url`, `description`
			FROM `project`
			ORDER BY `project_id` DESC
		');
	}
}