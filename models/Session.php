<?php

class Session
{
	public function __construct()
	{
		if (session_status() == PHP_SESSION_NONE)
		{
			session_start();
		}
	}

	public function login($username)
	{
		session_regenerate_id();

		$_SESSION['username'] = $username;
	}

	public function logout() {
		session_unset();
		session_destroy();
		header("Location: /project");
	}
}