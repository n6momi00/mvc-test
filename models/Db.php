<?php

class Db
{
	private static $settings = array(
		PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
        PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",
        PDO::ATTR_EMULATE_PREPARES => false
	);

	private static $conn;

	public static function connect($host, $user, $password, $database)
	{
		if(!isset(self::$conn))
		{
			try {
				self::$conn = new PDO(
					"mysql:host=$host;dbname=$database",
					$user,
					$password,
					self::$settings
				);	
			} catch (PDOException $e) {
				throw new PDOException($e->getMessage(), (int)$e->getCode());
			}
		}
	}

	public static function queryOne($query, $params = array())
	{
		$stmt = self::$conn->prepare($query);
        $stmt->execute($params);
        return $stmt->fetch();
	}

	public static function queryAll($query, $params = array())
	{
	    $stmt = self::$conn->prepare($query);
	    $stmt->execute($params);
	    return $stmt->fetchAll();
	}

	public static function querySingle($query, $params = array())
	{
	    $stmt = self::queryOne($query, $params);
	    if (!$stmt)
	    {
        	return false;
	    }
	    return $stmt[0];
	}

	public static function query($query, $params = array())
	{
        $stmt = self::$conn->prepare($query);
        $stmt->execute($params);
        return $stmt->rowCount();
	}

	public static function queryUser($query, $params = array(), $p)
	{
		$stmt = self::$conn->prepare($query);
		$stmt->execute($params);

		if ($stmt->rowCount() == 1)
		{
			$user = $stmt->fetch();
			$session = new Session();

			if (password_verify($p, $user['password']))
			{
				$session->login($user['username']);
				return true;
			}
			else
			{
				$_SESSION['msg'] = "<div class='alert alert-danger'>Username or password is incorrect!</div>";
				return false;
			}
		}
		else
		{
			$_SESSION['msg'] = "<div class='alert alert-danger'>Username or password is incorrect!</div>";
			return false;
		}
		
	}
}