<?php

class Login
{
	public function signin($username, $password)
	{
		return Db::queryUser('
			SELECT `username`, `password`
			FROM `user`
			WHERE `username` = ?
			LIMIT ?
		', array($username, 1), $password);
	}
}